package com.taniagoswami.a20210314_taniagoswami_nycschools.adapters;

public interface ISchoolClickListener {
    void schoolClicked(String schoolDbn);
}
