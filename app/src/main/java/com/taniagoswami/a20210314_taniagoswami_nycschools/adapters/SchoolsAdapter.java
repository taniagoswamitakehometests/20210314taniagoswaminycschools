package com.taniagoswami.a20210314_taniagoswami_nycschools.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.taniagoswami.a20210314_taniagoswami_nycschools.R;
import com.taniagoswami.a20210314_taniagoswami_nycschools.models.School;

public class SchoolsAdapter extends RecyclerView.Adapter<SchoolsAdapter.ViewHolder> {
    private School[] schools;
    private ISchoolClickListener schoolClickListener;

    public static class ViewHolder extends RecyclerView.ViewHolder {
        private final TextView textView;

        public ViewHolder(View view) {
            super(view);
            textView = view.findViewById(R.id.textView);
        }

        public TextView getTextView() {
            return textView;
        }

        public void bind(final School school, final ISchoolClickListener itemClickListener)
        {
            getTextView().setText(school.schoolName);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    itemClickListener.schoolClicked(school.dbn);
                }
            });
        }
    }

    public SchoolsAdapter(School[] schools, ISchoolClickListener itemClickListener) {
        this.schools = schools;
        this.schoolClickListener = itemClickListener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        View view = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.school_row_item, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, final int position) {
        viewHolder.bind(schools[position], schoolClickListener);
    }

    @Override
    public int getItemCount() {
        return schools.length;
    }

    public void updateData(School[] schools) {
        this.schools = schools;
    }
}