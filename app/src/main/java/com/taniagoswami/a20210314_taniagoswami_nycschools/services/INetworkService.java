package com.taniagoswami.a20210314_taniagoswami_nycschools.services;

import android.util.Pair;

import okhttp3.Callback;

interface INetworkService {
    void request(String urlString, Callback callback);
    void request(String urlString, Pair<String, String>[] params, Callback callback);
}
