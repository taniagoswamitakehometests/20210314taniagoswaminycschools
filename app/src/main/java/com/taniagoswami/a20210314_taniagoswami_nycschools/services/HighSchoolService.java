package com.taniagoswami.a20210314_taniagoswami_nycschools.services;

import android.util.Pair;

import com.google.gson.GsonBuilder;
import com.taniagoswami.a20210314_taniagoswami_nycschools.models.School;

import java.io.IOException;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.Response;

public class HighSchoolService {
    INetworkService networkService;
    String schoolsUrl = "https://data.cityofnewyork.us/resource/s3k6-pzi2.json";
    String scoreUrl = "https://data.cityofnewyork.us/resource/f9bf-2cp4.json";

    public HighSchoolService() {
        networkService = new NetworkService();
    }

    public void getHighSchools(final ISchoolsCallback callback) {
        Callback networkCallback = new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                callback.receivedFailure(e.getMessage());
            }

            @Override
            public void onResponse(Call call, final Response response) throws IOException {
                if (!response.isSuccessful()) {
                    throw new IOException("Unexpected code " + response);
                } else {
                    School[] schoolArray = new GsonBuilder().create().fromJson(response.body().string(), School[].class);
                    callback.receivedSchools(schoolArray);
                }
            }
        };
        networkService.request(schoolsUrl, networkCallback);
    }

    public void getTestScores(String schoolDbn, final ISchoolDataCallback callback) {
        Callback networkCallback = new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                callback.receivedFailure(e.getMessage());
            }

            @Override
            public void onResponse(Call call, final Response response) throws IOException {
                if (!response.isSuccessful()) {
                    throw new IOException("Unexpected code " + response);
                } else {
                    String json = response.body().string();
                    School[] schoolArray = new GsonBuilder().create().fromJson(json, School[].class);
                    if (schoolArray.length == 0) {
                        callback.receivedFailure("Unable to load scores for this school");
                    } else {
                        callback.receivedSchools(schoolArray[0]);
                    }
                }
            }
        };
        networkService.request(scoreUrl, new Pair[]{new Pair("dbn", schoolDbn)}, networkCallback);
    }
}
