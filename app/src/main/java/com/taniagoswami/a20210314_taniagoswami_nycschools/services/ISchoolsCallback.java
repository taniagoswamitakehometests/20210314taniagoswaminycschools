package com.taniagoswami.a20210314_taniagoswami_nycschools.services;

import com.taniagoswami.a20210314_taniagoswami_nycschools.models.School;

public interface ISchoolsCallback {
    void receivedSchools(School[] schools);
    void receivedFailure(String message);
}
