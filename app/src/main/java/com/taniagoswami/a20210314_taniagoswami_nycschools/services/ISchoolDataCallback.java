package com.taniagoswami.a20210314_taniagoswami_nycschools.services;

import com.taniagoswami.a20210314_taniagoswami_nycschools.models.School;

public interface ISchoolDataCallback {
    void receivedSchools(School school);
    void receivedFailure(String message);
}
