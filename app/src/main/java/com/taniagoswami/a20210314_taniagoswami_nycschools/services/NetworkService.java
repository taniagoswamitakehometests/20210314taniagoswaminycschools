package com.taniagoswami.a20210314_taniagoswami_nycschools.services;

import android.util.Pair;

import okhttp3.Callback;
import okhttp3.HttpUrl;
import okhttp3.OkHttpClient;
import okhttp3.Request;

public class NetworkService implements INetworkService {
    private OkHttpClient client;

    NetworkService() {
        client = new OkHttpClient();
    }

    public void request(String urlString, Callback callback) {
        Request request = new Request.Builder()
                .url(urlString)
                .build();

        client.newCall(request).enqueue(callback);
    }

    public void request(String urlString, Pair<String, String>[] params, Callback callback) {
        HttpUrl.Builder urlBuilder = HttpUrl.parse(urlString).newBuilder();
        for (int i=0; i<params.length; i++) {
            urlBuilder.addQueryParameter(params[i].first, params[i].second);
        }
        String url = urlBuilder.build().toString();

        Request request = new Request.Builder()
                .url(url)
                .build();

        client.newCall(request).enqueue(callback);
    }
}
