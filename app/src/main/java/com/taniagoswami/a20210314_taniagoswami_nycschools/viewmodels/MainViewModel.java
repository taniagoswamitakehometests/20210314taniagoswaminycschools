package com.taniagoswami.a20210314_taniagoswami_nycschools.viewmodels;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.MutableLiveData;

import com.taniagoswami.a20210314_taniagoswami_nycschools.SchoolsApplication;
import com.taniagoswami.a20210314_taniagoswami_nycschools.models.School;
import com.taniagoswami.a20210314_taniagoswami_nycschools.services.HighSchoolService;
import com.taniagoswami.a20210314_taniagoswami_nycschools.services.ISchoolDataCallback;
import com.taniagoswami.a20210314_taniagoswami_nycschools.services.ISchoolsCallback;

import java.util.Arrays;
import java.util.Comparator;

public class MainViewModel extends AndroidViewModel {
    private HighSchoolService highSchoolService = ((SchoolsApplication)getApplication()).highSchoolService;
    private MutableLiveData<School[]> schools;
    private MutableLiveData<School> schoolData;
    private MutableLiveData<String> errorMessage;

    public MainViewModel(@NonNull Application application) {
        super(application);
    }

    public MutableLiveData<School[]> getSchools() {
        if (schools == null) {
            schools = new MutableLiveData<School[]>();
        }
        return schools;
    }

    public MutableLiveData<School> getSchoolDetail() {
        if (schoolData == null) {
            schoolData = new MutableLiveData<School>();
        }
        return schoolData;
    }

    public MutableLiveData<String> errorMessage() {
        if (errorMessage == null) {
            errorMessage = new MutableLiveData<String>();
        }
        return errorMessage;
    }

    public void loadSchools() {
        ISchoolsCallback schoolsCallback = new ISchoolsCallback() {
            @Override
            public void receivedSchools(School[] receivedSchools) {
                Arrays.sort(receivedSchools, new Comparator<School>() {
                    @Override
                    public int compare(School s1, School s2) {
                        return s1.schoolName.compareTo(s2.schoolName);
                    }
                });
                schools.postValue(receivedSchools);
            }

            @Override
            public void receivedFailure(String message) {
                errorMessage.postValue(message);
            }
        };

        highSchoolService.getHighSchools(schoolsCallback);
    }

    public void loadIndividualSchool(String dbn) {
        ISchoolDataCallback schoolDataCallback = new ISchoolDataCallback() {
            @Override
            public void receivedSchools(School school) {
                schoolData.postValue(school);
            }

            @Override
            public void receivedFailure(String message) {
                errorMessage.postValue(message);
            }
        };

        highSchoolService.getTestScores(dbn, schoolDataCallback);
    }
}
