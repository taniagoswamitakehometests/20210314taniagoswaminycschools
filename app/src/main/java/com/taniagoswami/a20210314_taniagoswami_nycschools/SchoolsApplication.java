package com.taniagoswami.a20210314_taniagoswami_nycschools;

import android.app.Application;

import com.taniagoswami.a20210314_taniagoswami_nycschools.services.HighSchoolService;

public class SchoolsApplication extends Application {
    public HighSchoolService highSchoolService;

    public SchoolsApplication() {
        super();

        highSchoolService = new HighSchoolService();
    }
}
