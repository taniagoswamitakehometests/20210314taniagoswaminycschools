package com.taniagoswami.a20210314_taniagoswami_nycschools.activities;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.taniagoswami.a20210314_taniagoswami_nycschools.IntentConstants;
import com.taniagoswami.a20210314_taniagoswami_nycschools.R;
import com.taniagoswami.a20210314_taniagoswami_nycschools.adapters.ISchoolClickListener;
import com.taniagoswami.a20210314_taniagoswami_nycschools.adapters.SchoolsAdapter;
import com.taniagoswami.a20210314_taniagoswami_nycschools.models.School;
import com.taniagoswami.a20210314_taniagoswami_nycschools.viewmodels.MainViewModel;

import java.io.Serializable;

public class MainActivity extends AppCompatActivity {
    private MainViewModel mainViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final RecyclerView recyclerView = findViewById(R.id.schools_list);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        final SchoolsAdapter schoolsAdapter = new SchoolsAdapter(new School[]{}, new ISchoolClickListener() {
            @Override
            public void schoolClicked(String schoolDbn) {
                mainViewModel.loadIndividualSchool(schoolDbn);
            }
        });
        recyclerView.setAdapter(schoolsAdapter);

        mainViewModel = ViewModelProvider.AndroidViewModelFactory.getInstance(getApplication()).create(MainViewModel.class);

        final Observer<School[]> schoolsObserver = new Observer<School[]>() {
            @Override
            public void onChanged(@Nullable final School[] newSchools) {
                schoolsAdapter.updateData(newSchools);
                schoolsAdapter.notifyDataSetChanged();
            }
        };

        final Observer<School> schoolDataObserver = new Observer<School>() {
            @Override
            public void onChanged(@Nullable final School schoolData) {
                Intent intent = new Intent(MainActivity.this, SchoolDetailActivity.class);
                intent.putExtra(IntentConstants.SCHOOL_DATA, (Serializable) schoolData);
                startActivity(intent);
            }
        };

        final Observer<String> errorMessageObserver = new Observer<String>() {
            @Override
            public void onChanged(@Nullable final String errorMessage) {
                AlertDialog alertDialog = new AlertDialog.Builder(MainActivity.this).create();
                alertDialog.setTitle(R.string.error_title);
                alertDialog.setMessage(errorMessage);
                alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, getString(R.string.ok),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });
                alertDialog.show();
            }
        };

        mainViewModel.getSchools().observe(this, schoolsObserver);
        mainViewModel.getSchoolDetail().observe(this, schoolDataObserver);
        mainViewModel.errorMessage().observe(this, errorMessageObserver);

        mainViewModel.loadSchools();
    }
}
