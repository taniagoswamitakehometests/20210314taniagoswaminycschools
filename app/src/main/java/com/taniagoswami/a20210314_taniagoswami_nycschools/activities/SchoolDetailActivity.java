package com.taniagoswami.a20210314_taniagoswami_nycschools.activities;

import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.taniagoswami.a20210314_taniagoswami_nycschools.IntentConstants;
import com.taniagoswami.a20210314_taniagoswami_nycschools.R;
import com.taniagoswami.a20210314_taniagoswami_nycschools.models.School;

public class SchoolDetailActivity extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_school_detail);

        setTitle(R.string.sat_title);

        Intent intent = getIntent();
        School school = (School)intent.getSerializableExtra(IntentConstants.SCHOOL_DATA);

        TextView schoolNameTv = findViewById(R.id.school_name_tv);
        schoolNameTv.setText(school.schoolName);

        TextView mathScoreTv = findViewById(R.id.math_score_tv);
        mathScoreTv.setText(getString(R.string.math_score, school.math));

        TextView writingScoreTv = findViewById(R.id.writing_score_tv);
        writingScoreTv.setText(getString(R.string.writing_score, school.writing));

        TextView readingScoreTv = findViewById(R.id.reading_score_tv);
        readingScoreTv.setText(getString(R.string.reading_score, school.reading));
    }
}
