package com.taniagoswami.a20210314_taniagoswami_nycschools.models;

import com.google.gson.annotations.SerializedName;
import java.io.Serializable;

public class School implements Serializable {
    public String dbn;

    @SerializedName(value = "school_name")
    public String schoolName;

    @SerializedName(value = "sat_math_avg_score")
    public int math;

    @SerializedName(value = "sat_critical_reading_avg_score")
    public int reading;

    @SerializedName(value = "sat_writing_avg_score")
    public int writing;
}
